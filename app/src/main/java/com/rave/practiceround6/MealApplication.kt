package com.rave.practiceround6

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Application class for dagger hilt.
 *
 * @constructor Create empty Meal application
 */
@HiltAndroidApp
class MealApplication : Application()
