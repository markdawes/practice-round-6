package com.rave.practiceround6.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.practiceround6.model.MealRepo
import com.rave.practiceround6.model.local.Category
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Main view model.
 *
 * @property repo
 * @constructor Create empty Meal view model
 */
@HiltViewModel
class MealViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {
    private var _categories: MutableLiveData<List<Category>> = MutableLiveData()
    val categories: LiveData<List<Category>> get() = _categories

    init {
        getMealCategories()
    }

    /**
     * Get meal categories.
     *
     */
    private fun getMealCategories() = viewModelScope.launch {
        _categories.value = repo.getMealCategories()
    }
}
