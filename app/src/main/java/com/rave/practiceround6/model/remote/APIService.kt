package com.rave.practiceround6.model.remote

import com.rave.practiceround6.model.remote.dtos.CategoryResponse
import retrofit2.http.GET

/**
 * provides category endpoints.
 *
 * @constructor Create empty A p i service
 */
interface APIService {

    @GET(CATEGORY_ENDPOINT)
    suspend fun getMealCategories(): CategoryResponse

    companion object {
        private const val CATEGORY_ENDPOINT = "categories.php"
    }
}
