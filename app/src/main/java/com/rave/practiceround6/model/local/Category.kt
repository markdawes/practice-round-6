package com.rave.practiceround6.model.local

/**
 * Category.
 *
 * @property idCategory
 * @property strCategory
 * @property strCategoryDescription
 * @property strCategoryThumb
 * @constructor Create empty Category
 */
data class Category(
    val idCategory: String,
    val strCategory: String,
    val strCategoryDescription: String,
    val strCategoryThumb: String
)
