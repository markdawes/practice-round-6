package com.rave.practiceround6.model

import com.rave.practiceround6.model.local.Category
import com.rave.practiceround6.model.remote.APIService
import javax.inject.Inject

/**
 * Meal repo.
 *
 * @property service
 * @constructor Create empty Meal repo
 */
class MealRepo @Inject constructor(private val service: APIService) {

    /**
     * Get meal categories.
     *
     * @return
     */
    suspend fun getMealCategories(): List<Category> {
        val categoryDTOs = service.getMealCategories().categories
        return categoryDTOs.map {
            Category(
                idCategory = it.idCategory,
                strCategory = it.strCategory,
                strCategoryThumb = it.strCategoryThumb,
                strCategoryDescription = it.strCategoryDescription
            )
        }
    }
}
